/**
 * @format
 */

declare type ColorType = {
  backgroundColor: string;
  active: string;
  white: string;
  black: string;
};

export const color: ColorType = {
  backgroundColor: '#EFEFEF',
  active: '#F00',
  white: '#FFF',
  black: '#000',
};
