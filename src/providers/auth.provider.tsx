/**
 * @format
 */

import React, { createContext, useEffect, useState } from 'react';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { API_URL } from '../constants/constants';
import axios from 'axios';

export const AuthContext = createContext<any>(undefined);

const AuthProvider = (props: any) => {
  const { children } = props;

  const [currentUser, setCurrentUser] = useState<Object | null>();

  const isLoggedIn = async () => {
    const user = await AsyncStorage.getItem('token');
    const _currentUser = await AsyncStorage.getItem('currentUser');
    if (user && _currentUser) {
      setCurrentUser(JSON.parse(_currentUser));
    }
  };

  const onLogout = async () => {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('currentUser');
    setCurrentUser(null);
  };

  const onLogin = async (email: string, password: string) => {
    const payload = { email, password };
    try {
      const user = await axios.post(`${API_URL}/auth/login`, payload);
      if (user) {
        const result = await axios.get(`${API_URL}/user/profile`, {
          headers: {
            Authorization: `Bearer ${user.data.accessToken}`,
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        });
        await AsyncStorage.setItem('token', user.data.accessToken);
        await AsyncStorage.setItem('currentUser', JSON.stringify(result.data));
        setCurrentUser(result.data);
      }
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  useEffect(() => {
    setTimeout(() => isLoggedIn(), 1000);
  }, []);

  return (
    <AuthContext.Provider
      value={{ currentUser, onLogout, onLogin, isLoggedIn }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
