/**
 * @format
 */

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '@screens/home';
import FilmDetail from '@screens/film-detail';

const HomeStack = createStackNavigator();

const HomeStackNavigator = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name={'Home'}
        component={Home}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name={'FilmDetail'}
        component={FilmDetail}
        options={{ headerShown: false }}
      />
    </HomeStack.Navigator>
  );
};

export default HomeStackNavigator;
