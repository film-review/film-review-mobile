/**
 * @format
 */

import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../screens/home';
import Account from '../screens/account';
import { Image } from 'react-native';
import HomeStackNavigator from './home.stack.navigator';
import AccountStackNavigator from './account.stack.navigator';

const Tab = createBottomTabNavigator();

const R = {
  home: require('@assets/icons/ic_home.png'),
  account: require('@assets/icons/ic_account.png'),
};

const TabNavigator = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: 'red',
        showLabel: false,
      }}>
      <Tab.Screen
        name={'HomeStackNavigator'}
        component={HomeStackNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <Image source={R.home} style={{ tintColor: color }} />
          ),
          title: 'Trang chủ',
        }}
      />
      <Tab.Screen
        name={'AccountStackNavigator'}
        component={AccountStackNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <Image source={R.account} style={{ tintColor: color }} />
          ),
          title: 'Tài khoản',
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
