/**
 * @format
 */

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/login';
import Register from '../screens/register';

const AuthStack = createStackNavigator();

const AuthStackNavigator = () => {
  return (
    <AuthStack.Navigator>
      <AuthStack.Screen
        name={'Login'}
        options={{
          headerShown: false,
        }}
        component={() => <Login />}
      />
      <AuthStack.Screen
        name={'Register'}
        options={{
          headerShown: false,
        }}
        component={() => <Register />}
      />
    </AuthStack.Navigator>
  );
};

export default AuthStackNavigator;
