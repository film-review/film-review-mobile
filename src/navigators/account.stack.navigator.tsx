/**
 * @format
 */

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Account from '@screens/account';
import UpdateInfo from '@screens/update-info';

const AccountStack = createStackNavigator();

const AccountStackNavigator = () => {
  return (
    <AccountStack.Navigator>
      <AccountStack.Screen
        name={'Account'}
        component={Account}
        options={{
          headerShown: false,
        }}
      />
      <AccountStack.Screen
        name={'UpdateInfo'}
        component={UpdateInfo}
        options={{
          headerShown: false,
        }}
      />
    </AccountStack.Navigator>
  );
};

export default AccountStackNavigator;
