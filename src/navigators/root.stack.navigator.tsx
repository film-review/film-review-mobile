/**
 * @format
 */

import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../screens/splash';
import TabNavigator from './tab.navigator';
import Login from '@screens/login';

const RootStack = createStackNavigator();

const RootStackNavigator = () => {
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  return (
    <NavigationContainer>
      <RootStack.Navigator>
        {loading ? (
          <RootStack.Screen
            name={'Splash'}
            component={Splash}
            options={{
              headerShown: false,
              gestureEnabled: false,
            }}
          />
        ) : (
          <RootStack.Screen
            name={'TabNavigator'}
            component={TabNavigator}
            options={{
              headerShown: false,
              gestureEnabled: false,
            }}
          />
        )}
        <RootStack.Screen
          name={'Login'}
          component={Login}
          options={{
            headerShown: false,
          }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default RootStackNavigator;
