/**
 * @format
 */

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TabNavigator from './tab.navigator';

const AppStack = createStackNavigator();

const AppStackNavigator = () => {
  return (
    <AppStack.Navigator>
      <AppStack.Screen
        name={'TabNavigator'}
        component={TabNavigator}
        options={{ headerShown: false, gestureEnabled: false }}
      />
    </AppStack.Navigator>
  );
};

export default AppStackNavigator;
