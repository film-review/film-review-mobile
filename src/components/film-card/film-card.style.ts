/**
 * @format
 */
import { Dimensions, StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      backgroundColor: color.white,
      padding: 8,
      borderRadius: 10,
      elevation: 1,
    },
    imageStyle: {
      width: Dimensions.get('screen').width - 48,
      height: 200,
      borderRadius: 10,
    },
    titleStyle: {
      marginTop: 16,
      fontWeight: 'bold',
      fontSize: 18,
    },
    ratingWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },
  });
};

export default useStyles;
