/**
 * @format
 */

import React from 'react';
import FilmCardPresenter from '@components/film-card/film-card.presenter';
import { Props } from './film-card.typed';

const FilmCard = (props: Props) => {
  return <FilmCardPresenter {...props} />;
};

export default FilmCard;
