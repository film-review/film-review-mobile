/**
 * @format
 */

import React from 'react';
import FilmCardView from '@components/film-card/film-card.view';
import { Props } from './film-card.typed';

const FilmCardPresenter = (props: Props) => {
  return <FilmCardView {...props} />;
};

export default FilmCardPresenter;
