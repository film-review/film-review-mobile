/**
 * @format
 */

import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import useStyles from './film-card.style';
import { Props } from './film-card.typed';
import { Rating } from 'react-native-ratings';

const FilmCardView = (props: Props) => {
  const { style, filmThumbnailUri, rating, readonly, title, onPress } = props;
  const styles = useStyles();

  return (
    <TouchableOpacity style={[styles.root, style]} onPress={onPress}>
      <Image
        source={{ uri: filmThumbnailUri || null }}
        style={styles.imageStyle}
      />
      <Text style={styles.titleStyle}>{title}</Text>
      <View style={styles.ratingWrapper}>
        <Text style={{ fontSize: 14 }}>Đánh giá: </Text>
        <Rating
          type={'star'}
          ratingCount={5}
          fractions={1}
          imageSize={15}
          startingValue={rating}
          onFinishRating={(number) => console.log(number)}
          readonly={readonly}
        />
      </View>
    </TouchableOpacity>
  );
};

export default FilmCardView;
