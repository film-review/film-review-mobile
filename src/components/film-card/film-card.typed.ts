/**
 * @format
 */
import { ViewStyle } from 'react-native';

export declare type ViewProps = {};
export declare type Props = {
  style?: ViewStyle;
  filmThumbnailUri: string;
  rating: number;
  readonly?: boolean;
  title: string;
  onPress?: () => void;
};
