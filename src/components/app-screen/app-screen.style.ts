/**
 * @format
 */

import { StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
    },
    navbar: {
      backgroundColor: color.backgroundColor,
      width: '100%',
      height: 50,
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingHorizontal: 16,
      flexDirection: 'row',
    },
    titleWrapper: {
      position: 'absolute',
      left: 0,
      right: 0,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontWeight: 'bold',
      fontSize: 16,
    },
  });
};

export default useStyles;
