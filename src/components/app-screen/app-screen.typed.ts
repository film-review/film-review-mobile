/**
 * @format
 */

import { ImageProps, ViewStyle, TextProps, ImageStyle } from 'react-native';

declare type leftViewProps = {
  icon: ImageProps;
  onPress?: () => void;
  iconStyle?: ImageStyle;
};

declare type rightViewProps = {
  icon: ImageProps;
  onPress: () => void;
  iconStyle?: ImageStyle;
};

export declare type Props = {
  children?: any;
  style?: ViewStyle;
  leftView?: leftViewProps;
  rightView?: rightViewProps;
  title: string;
  titleStyle?: TextProps;
};
