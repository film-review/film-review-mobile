/**
 * @format
 */

import React from 'react';
import { Props } from './app-screen.typed';
import AppScreenPresenter from './app-screen.presenter';

const AppScreen = (props: Props) => {
  return <AppScreenPresenter {...props} />;
};

export default AppScreen;
