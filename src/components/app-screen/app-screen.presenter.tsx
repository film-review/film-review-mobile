/**
 * @format
 */

import React from 'react';
import { Props } from './app-screen.typed';
import AppScreenView from './app-screen.view';

const AppScreenPresenter = (props: Props) => {
  return <AppScreenView {...props} />;
};

export default AppScreenPresenter;
