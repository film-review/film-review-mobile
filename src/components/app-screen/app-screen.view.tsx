/**
 * @format
 */

import React from 'react';
import { Props } from './app-screen.typed';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import useStyles from './app-screen.style';

const R = {
  leftArrow: require('@assets/icons/ic_arrow.png'),
};

const AppScreenView = (props: Props) => {
  const { children, style, title, leftView, rightView, titleStyle } = props;
  const styles = useStyles();

  return (
    <SafeAreaView style={[styles.root]}>
      <View style={styles.navbar}>
        {leftView ? (
          <TouchableOpacity onPress={leftView?.onPress}>
            <Image
              source={leftView.icon ? leftView.icon : R.leftArrow}
              style={leftView?.iconStyle}
            />
          </TouchableOpacity>
        ) : (
          <View />
        )}

        <View style={styles.titleWrapper}>
          <Text style={[styles.title, titleStyle]}>{title || ''}</Text>
        </View>

        {rightView ? (
          <TouchableOpacity onPress={rightView?.onPress}>
            <Image
              source={rightView.icon ? rightView.icon : R.leftArrow}
              style={rightView?.iconStyle}
            />
          </TouchableOpacity>
        ) : (
          <View />
        )}
      </View>
      <View style={style}>{children}</View>
    </SafeAreaView>
  );
};

export default AppScreenView;
