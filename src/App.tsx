/**
 * @format
 */

import React from 'react';
import RootStackNavigator from './navigators/root.stack.navigator';
import AuthProvider from './providers/auth.provider';

const App = () => {
  return (
    <AuthProvider>
      <RootStackNavigator />
    </AuthProvider>
  );
};

export default App;
