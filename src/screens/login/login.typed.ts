/**
 * @format
 */

export declare type ViewProps = {
  onLogin: (email: string, password: string) => void;
};
export declare type Props = {};
