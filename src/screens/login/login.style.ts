/**
 * @format
 */

import { StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      backgroundColor: color.white,
      paddingHorizontal: 32,
    },
    emailInput: {
      width: '100%',
      borderRadius: 5,
      paddingVertical: 5,
      paddingLeft: 16,
      marginTop: 32,
      backgroundColor: color.backgroundColor,
    },
    passwordInput: {
      width: '100%',
      borderRadius: 5,
      marginTop: 16,
      paddingVertical: 5,
      paddingLeft: 16,
      backgroundColor: color.backgroundColor,
    },
    loginButton: {
      width: '100%',
      marginTop: 16,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: color.active,
      borderRadius: 5,
      paddingVertical: 10,
    },
    registerButton: {
      width: '100%',
      marginTop: 16,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 2,
      borderColor: color.active,
      borderRadius: 20,
      paddingVertical: 5,
      backgroundColor: color.white,
    },
    formArea: {},
  });
};

export default useStyles;
