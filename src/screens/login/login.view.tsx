/**
 * @format
 */

import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import useStyles from './login.style';
import { ViewProps } from './login.typed';

const LoginView = (props: ViewProps) => {
  const { onLogin } = props;
  const styles = useStyles();

  const [email, setEmail] = useState<string>('string');
  const [password, setPassword] = useState<string>('string');

  return (
    <View style={styles.root}>
      <Text style={{ fontSize: 25, marginTop: 128 }}>Đăng nhập</Text>

      <View style={styles.formArea}>
        <TextInput
          value={email}
          placeholder={'Email'}
          style={styles.emailInput}
          onChangeText={setEmail}
        />
        <TextInput
          value={password}
          placeholder={'Password'}
          style={styles.passwordInput}
          onChangeText={setPassword}
          secureTextEntry
        />
        <TouchableOpacity
          onPress={() => onLogin(email, password)}
          style={styles.loginButton}>
          <Text style={{ color: '#FFF' }}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginView;
