/**
 * @format
 */

import React from 'react';
import LoginView from './login.view';
import { Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import useAuth from '../../hooks/useAuth';

const LoginPresenter = () => {
  const { goBack } = useNavigation();
  const { onLogin } = useAuth();

  const _onLogin = (email: string, password: string) => {
    if (!email || !password) {
      Alert.alert('Email hoặc mật khẩu không được để trống' + '');
      return;
    }

    onLogin(email, password);
    goBack();
  };

  return <LoginView onLogin={_onLogin} />;
};

export default LoginPresenter;
