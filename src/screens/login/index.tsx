/**
 * @format
 */

import React from 'react';
import LoginPresenter from './login.presenter';

const Login = () => {
  return <LoginPresenter />;
};

Login.navigationOptions = {
  headerShown: false,
};

export default Login;
