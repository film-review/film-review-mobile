/**
 * @format
 */

export declare type ViewProps = {
  onRefresh: () => void;
  refreshing: boolean;
  data: Array<Object>;
};

export declare type Props = {};
