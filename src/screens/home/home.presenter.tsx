/**
 * @format
 */

import React, { useEffect, useState } from 'react';
import HomeView from './home.view';
import { Props } from './home.typed';
import { Alert } from 'react-native';
import axios from 'axios';
import { API_URL } from '../../constants/constants';

const HomePresenter = (props: Props) => {
  const [refreshing, setRefreshing] = useState(false);
  const [data, setData] = useState([]);

  const _getListFilm = async () => {
    try {
      const result = await axios.get(`${API_URL}/film`);
      setData(result.data);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  const _onRefresh = () => {
    setRefreshing(true);
    _getListFilm().then(() => setRefreshing(false));
  };

  useEffect(() => {
    _getListFilm();
  }, []);

  return (
    <HomeView
      {...props}
      data={data}
      onRefresh={_onRefresh}
      refreshing={refreshing}
    />
  );
};

export default HomePresenter;
