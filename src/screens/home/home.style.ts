/**
 * @format
 */

import { StyleSheet } from 'react-native';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      alignItems: 'center',
    },
    leftIcon: {
      width: 30,
      height: 30,
    },
  });
};

export default useStyles;
