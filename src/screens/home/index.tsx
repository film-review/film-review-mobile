/**
 * @format
 */

import React from 'react';
import HomePresenter from './home.presenter';
import { Props } from './home.typed';

const Home = (props: Props) => {
  return <HomePresenter {...props} />;
};

Home.navigationOptions = {
  headerShown: false,
};

export default Home;
