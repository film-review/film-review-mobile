/**
 * @format
 */

import React from 'react';
import useStyles from './home.style';
import AppScreen from '@components/app-screen';
import FilmCard from '@components/film-card';
import { FlatList } from 'react-native';
import { ViewProps } from './home.typed';
import { useNavigation } from '@react-navigation/native';

const R = {
  leftArrow: require('@assets/icons/ic_arrow.png'),
  logo: require('@assets/icons/logo.png'),
};

const HomeView = (props: ViewProps) => {
  const { onRefresh, refreshing, data } = props;
  const styles = useStyles();
  const { navigate } = useNavigation();

  const leftView = {
    icon: R.logo,
    onPress: () => {},
    iconStyle: styles.leftIcon,
  };

  const renderFilmCard = ({ item, index }: any) => {
    return (
      <FilmCard
        key={index.toString()}
        filmThumbnailUri={data?.thumbnail}
        style={{ marginBottom: 16 }}
        rating={item.rating}
        readonly
        title={item.name}
        onPress={() => navigate('FilmDetail', { id: item.id })}
      />
    );
  };

  return (
    <AppScreen style={styles.root} title={'Film Review'} leftView={leftView}>
      <FlatList
        data={data}
        contentContainerStyle={{ paddingHorizontal: 16, paddingTop: 16 }}
        renderItem={renderFilmCard}
        keyExtractor={(item, index) => `${index}`}
        onRefresh={onRefresh}
        refreshing={refreshing}
      />
    </AppScreen>
  );
};

export default HomeView;
