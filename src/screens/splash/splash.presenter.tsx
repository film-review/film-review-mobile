/**
 * @format
 */

import React from 'react';
import SplashView from './splash.view';

const SplashPresenter = () => {
  return <SplashView />;
};

export default SplashPresenter;
