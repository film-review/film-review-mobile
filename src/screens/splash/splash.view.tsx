/**
 * @format
 */

import React from 'react';
import { View, Text, Image, ActivityIndicator } from 'react-native';
import useStyles from './splash.style';

const R = {
  logo: require('../../../assets/icons/logo.png'),
};

const SplashView = () => {
  const styles = useStyles();

  return (
    <View style={styles.root}>
      <Image source={R.logo} style={styles.logo} />
      <Text style={styles.title}>Film Review</Text>
      <ActivityIndicator
        size={'large'}
        style={{ position: 'absolute', bottom: 64 }}
        color={'#FFF'}
      />
    </View>
  );
};

export default SplashView;
