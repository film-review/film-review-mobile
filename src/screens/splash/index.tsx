/**
 * @format
 */

import React from 'react';
import SplashPresenter from './splash.presenter';

const Splash = () => {
  return <SplashPresenter />;
};

Splash.navigationOptions = {
  headerShown: false,
};

export default Splash;
