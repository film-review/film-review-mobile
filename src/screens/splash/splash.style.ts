/**
 * @format
 */

import { StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'red',
    },
    logo: {
      width: 100,
      height: 100,
      marginTop: '50%',
    },
    title: {
      fontSize: 30,
      marginTop: 16,
      color: '#FFF',
    },
  });
};

export default useStyles;
