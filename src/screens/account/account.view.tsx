/**
 * @format
 */

import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import useStyles from './account.style';
import useAuth from '../../hooks/useAuth';
import AppScreen from '@components/app-screen';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import type { ViewProps } from './account.typed';

const R = {
  leftArrow: require('@assets/icons/ic_arrow.png'),
  email: require('@assets/icons/ic_email.png'),
  logout: require('@assets/icons/ic_signs.png'),
  logo: require('@assets/icons/logo.png'),
};

const AccountView = (props: ViewProps) => {
  const { profile, onRefresh } = props;
  const styles = useStyles();
  const { navigate } = useNavigation();
  const { currentUser, onLogout } = useAuth();

  const leftView = {
    icon: R.logo,
    onPress: () => {},
    iconStyle: styles.leftIcon,
  };

  const loginView = (
    <View style={styles.loginWrapper}>
      <Text style={styles.message}>
        Vui lòng đăng nhập để đánh giá phim mà bạn đã trải nghiệm
      </Text>
      <TouchableOpacity onPress={() => navigate('Login')} style={styles.login}>
        <Image source={R.email} style={{ tintColor: '#FFF', marginRight: 8 }} />
        <Text style={{ color: '#FFF' }}>Email</Text>
      </TouchableOpacity>
    </View>
  );

  const placeholderAvatar = (
    <View style={styles.avatar}>
      <Text style={styles.avatarText}>
        {profile?.email?.split('')[0].toUpperCase()}
      </Text>
    </View>
  );

  const accountView = (
    <View style={styles.accountAreaWrapper}>
      <View style={styles.accountArea}>
        {!!profile?.avatar ? (
          <Image source={{ uri: profile?.avatar }} style={styles.avatar} />
        ) : (
          placeholderAvatar
        )}
        <Text style={styles.displayName}>{profile?.displayName || ''}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            paddingHorizontal: 64,
            marginTop: 16,
          }}>
          <View>
            <Text>Email</Text>
            <Text>{profile?.email}</Text>
          </View>
          <View>
            <Text>Ngày sinh</Text>
            <Text>{moment(profile?.dateOfBirth).format('DD-MM-YYYY')}</Text>
          </View>
        </View>
      </View>
    </View>
  );

  return (
    <AppScreen style={styles.root} title={'Tài khoản'} leftView={leftView}>
      {!currentUser && loginView}
      {!!currentUser && accountView}
      {!!currentUser && (
        <TouchableOpacity onPress={onLogout} style={styles.logout}>
          <Image source={R.logout} style={{ tintColor: '#FFF' }} />
        </TouchableOpacity>
      )}

      {!!currentUser && (
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigate('UpdateInfo', { onRefresh })}>
          <Text>Cập nhật thông tin</Text>
        </TouchableOpacity>
      )}
    </AppScreen>
  );
};

export default AccountView;
