/**
 * @format
 */

import React, { useEffect, useState } from 'react';
import AccountView from './account.view';
import { Props } from './account.typed';
import { Alert } from 'react-native';
import { API_URL } from '../../constants/constants';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import useAuth from '../../hooks/useAuth';

const AccountPresenter = (props: Props) => {
  const [profile, setProfile] = useState({});

  const getProfile = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (!token) return;
      const result = await axios.get(`${API_URL}/user/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      await AsyncStorage.setItem('currentUser', JSON.stringify(result.data));
      setProfile(result.data);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  const onRefresh = () => {
    getProfile();
  };

  useEffect(() => {
    getProfile();
  }, [profile]);

  return <AccountView profile={profile} onRefresh={onRefresh} />;
};

export default AccountPresenter;
