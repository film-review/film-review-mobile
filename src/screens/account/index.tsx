/**
 * @format
 */

import React from 'react';
import AccountPresenter from './account.presenter';
import { Props } from './account.typed';

const Account = (props: Props) => {
  return <AccountPresenter {...props} />;
};

Account.navigationOptions = {
  headerShown: false,
};

export default Account;
