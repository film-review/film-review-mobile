/**
 * @format
 */

import { StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      alignItems: 'center',
      paddingHorizontal: 16,
    },
    leftIcon: {
      width: 30,
      height: 30,
    },
    logout: {
      position: 'absolute',
      bottom: 32,
      right: 32,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 24,
      backgroundColor: color.active,
      width: 48,
      height: 48,
      elevation: 5,
    },
    avatar: {
      backgroundColor: 'red',
      width: 64,
      height: 64,
      borderRadius: 32,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: -32,
    },
    avatarText: {
      fontSize: 30,
      color: '#FFF',
    },
    displayName: {
      marginTop: 32,
      fontSize: 20,
    },
    login: {
      flexDirection: 'row',
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 10,
      marginTop: 16,
      borderRadius: 5,
      marginHorizontal: 32,
    },
    message: {
      textAlign: 'center',
      marginHorizontal: 32,
    },
    loginWrapper: {
      justifyContent: 'center',
      flex: 1,
    },
    accountArea: {
      backgroundColor: '#FFF',
      width: '100%',
      alignItems: 'center',
      borderRadius: 10,
      elevation: 5,
      height: 128,
    },
    accountAreaWrapper: {
      alignItems: 'center',
      marginTop: 64,
      width: '100%',
    },
    button: {
      backgroundColor: '#FFF',
      width: '100%',
      alignItems: 'center',
      borderRadius: 10,
      paddingVertical: 8,
      elevation: 5,
      marginTop: 16,
    },
  });
};

export default useStyles;
