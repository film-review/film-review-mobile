/**
 * @format
 */

import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import useStyles from './update-info.style';
import { ViewProps } from './update-info.typed';
import moment from 'moment';

const UpdateInfoView = (props: ViewProps) => {
  const styles = useStyles();
  const { profile, onUpdate } = props;
  const { displayName = '', dateOfBirth = new Date(), fullName = '' } = profile;

  const [displayNameState, setDisplayName] = useState<string>(displayName);
  const [dateOfBirthState, setDateOfBirth] = useState<string>(dateOfBirth);
  const [fullNameState, setFullName] = useState<string>(fullName);

  return (
    <View style={styles.root}>
      <Text style={{ fontSize: 25, marginTop: 128 }}>Cập nhật</Text>

      <View style={styles.formArea}>
        <TextInput
          value={displayNameState}
          placeholder={'Tên hiển thị'}
          style={styles.emailInput}
          onChangeText={setDisplayName}
        />
        <TextInput
          value={fullNameState}
          placeholder={'Họ và tên'}
          style={styles.passwordInput}
          onChangeText={setFullName}
        />
        <TouchableOpacity
          onPress={() => onUpdate({ displayNameState, fullNameState })}
          style={styles.loginButton}>
          <Text style={{ color: '#FFF' }}>Cập nhật</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default UpdateInfoView;
