/**
 * @format
 */

export declare type ViewProps = {
  onUpdate: (item: any) => void;
  profile: any;
};
export declare type Props = {};
