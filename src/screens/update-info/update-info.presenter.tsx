/**
 * @format
 */

import React, { useEffect, useState } from 'react';
import UpdateInfoView from './update-info.view';
import { Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { API_URL } from '../../constants/constants';

const UpdateInfoPresenter = ({ route }) => {
  const { goBack } = useNavigation();
  const [profile, setProfile] = useState({});
  const onRefresh = route.params.onRefresh;

  const _getProfile = async () => {
    try {
      const token: string | null = await AsyncStorage.getItem('token');
      const result = await axios.get(`${API_URL}/user/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      setProfile(result.data);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  const _onUpdate = async ({ displayNameState, fullNameState }) => {
    if (!profile.id) return;
    if (!displayNameState || !fullNameState) return;
    const payload: any = {
      displayName: displayNameState,
      fullName: fullNameState,
    };
    try {
      const token: string | null = await AsyncStorage.getItem('token');
      if (!token) {
        Alert.alert('Lỗi', 'Chưa đăng nhập');
        return;
      }
      const result = await axios.put(
        `${API_URL}/user/${profile.id}/update-info`,
        payload,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        },
      );
      console.log(result.data);
      onRefresh();
      goBack();
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  useEffect(() => {
    _getProfile();
  }, []);

  return <UpdateInfoView profile={profile} onUpdate={_onUpdate} />;
};

export default UpdateInfoPresenter;
