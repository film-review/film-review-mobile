/**
 * @format
 */

import React from 'react';
import UpdateInfoPresenter from './update-info.presenter';

const UpdateInfo = (props) => {
  return <UpdateInfoPresenter {...props} />;
};

UpdateInfo.navigationOptions = {
  headerShown: false,
};

export default UpdateInfo;
