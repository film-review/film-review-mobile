/**
 * @format
 */

export declare type ViewProps = {
  onRegister: (email: string, password: string) => void;
};
export declare type Props = {};
