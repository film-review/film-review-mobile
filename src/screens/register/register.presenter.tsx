/**
 * @format
 */

import React from 'react';
import RegisterView from './register.view';
import auth from '@react-native-firebase/auth';
import { Alert } from 'react-native';

const RegisterPresenter = () => {
  const onRegister = async (email: string, password: string) => {
    if (!email || !password) {
      Alert.alert('Email hoặc mật khẩu không được để trống' + '');
      return;
    }

    try {
      await auth().createUserWithEmailAndPassword(email, password);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  return <RegisterView onRegister={onRegister} />;
};

export default RegisterPresenter;
