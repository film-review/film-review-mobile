/**
 * @format
 */

import { StyleSheet } from 'react-native';
import { color } from '@common/color';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      backgroundColor: color.backgroundColor,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 32,
    },
    emailInput: {
      backgroundColor: '#FFF',
      width: '100%',
      borderRadius: 20,
      textAlign: 'center',
      paddingVertical: 5,
    },
    passwordInput: {
      backgroundColor: '#FFF',
      width: '100%',
      borderRadius: 20,
      marginTop: 16,
      textAlign: 'center',
      paddingVertical: 5,
    },
    registerButton: {
      width: '100%',
      marginTop: 16,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: color.active,
      borderRadius: 20,
      paddingVertical: 5,
    },
  });
};

export default useStyles;
