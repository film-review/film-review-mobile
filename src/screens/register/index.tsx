/**
 * @format
 */

import React from 'react';
import RegisterPresenter from './register.presenter';

const Register = () => {
  return <RegisterPresenter />;
};

Register.navigationOptions = {
  headerShown: false,
};

export default Register;
