/**
 * @format
 */

import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import useStyles from './register.style';
import { ViewProps } from './register.typed';

const RegisterView = (props: ViewProps) => {
  const { onRegister } = props;
  const styles = useStyles();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  return (
    <View style={styles.root}>
      <TextInput
        value={email}
        placeholder={'Email'}
        style={styles.emailInput}
        onChangeText={setEmail}
      />
      <TextInput
        value={password}
        placeholder={'Password'}
        style={styles.passwordInput}
        onChangeText={setPassword}
      />
      <TouchableOpacity
        onPress={() => onRegister(email, password)}
        style={styles.registerButton}>
        <Text style={{ color: '#FFF' }}>Register</Text>
      </TouchableOpacity>
    </View>
  );
};

export default RegisterView;
