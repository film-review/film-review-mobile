/**
 * @format
 */

export declare type Director = {
  id: number;
  fullName: string;
  dateOfBirth: Date;
};

export declare type filmDirectors = {
  director: Director;
};

export declare type filmCasts = {
  cast: cast;
};

export declare type cast = {
  id: number;
  fullName: string;
  dateOfBirth: Date;
  avatar?: string;
};

export declare type reviewType = {
  id: number;
  content: string;
};

export declare type DataPropTypes = {
  premiere: string;
  rating: number;
  duration: number;
  name: string;
  filmCategories: {
    category: {
      id: number;
      name: string;
    };
  };
  filmDirectors: Array<filmDirectors>;
  filmCasts: filmCasts[];
  reviews: reviewType[];
};

export declare type ViewProps = {
  data: DataPropTypes | any;
  onSend: () => void;
  onChange: (text: string) => void;
  comment: string;
  reviews: reviewType[];
};
