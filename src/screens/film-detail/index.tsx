/**
 * @format
 */

import React from 'react';
import FilmDetailPresenter from './film-detail.presenter';

const FilmDetail = (props) => {
  return <FilmDetailPresenter {...props} />;
};

FilmDetail.navigationOptions = {
  headerShown: false,
};

export default FilmDetail;
