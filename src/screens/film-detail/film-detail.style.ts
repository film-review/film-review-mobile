/**
 * @format
 */
import { StyleSheet } from 'react-native';

const useStyles = () => {
  return StyleSheet.create({
    root: {
      flex: 1,
      paddingHorizontal: 16,
    },
    leftIcon: {
      width: 20,
      height: 20,
    },
    titleStyle: {
      fontWeight: 'bold',
    },
    castTitle: {
      marginTop: 16,
      fontWeight: 'bold',
      fontSize: 16,
    },
    castItem: {
      marginRight: 16,
      backgroundColor: '#FFF',
      padding: 10,
      borderRadius: 5,
      width: 95,
      justifyContent: 'center',
    },
    sendButton: {
      width: 50,
      height: 50,
      backgroundColor: '#FFF',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 5,
    },
    inputStyle: {
      height: 50,
      backgroundColor: '#FFF',
      borderRadius: 5,
      flex: 1,
      marginRight: 8,
    },
    commentItem: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 16,
      backgroundColor: '#FFF',
      padding: 5,
      borderRadius: 5,
    },
    avatarStyle: {
      width: 50,
      aspectRatio: 1,
      borderRadius: 25,
    },
    thumbnailWrapper: {
      backgroundColor: '#FFF',
      padding: 16,
      flexDirection: 'row',
      borderRadius: 5,
    },
  });
};

export default useStyles;
