/**
 * @format
 */

import React from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AppScreen from '@components/app-screen';
import useStyles from './film-detail.style';
import type { ViewProps } from './film-detail.typed';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';

const R = {
  leftArrow: require('@assets/icons/ic_arrow.png'),
  logo: require('@assets/icons/logo.png'),
};

const FilmDetailView = (props: ViewProps) => {
  const { data, onSend, onChange, comment, reviews } = props;
  const styles = useStyles();
  const { goBack } = useNavigation();

  const leftView = {
    icon: R.leftArrow,
    onPress: () => goBack(),
    iconStyle: styles.leftIcon,
  };

  return (
    <AppScreen style={{ flex: 1 }} leftView={leftView} title={data?.name}>
      <ScrollView
        style={styles.root}
        contentContainerStyle={{ paddingBottom: 32 }}>
        <View style={styles.thumbnailWrapper}>
          <Image
            source={{ uri: data?.thumbnail }}
            style={{ width: 150, height: 200 }}
          />
          <View style={{ marginLeft: 16 }}>
            <Text style={styles.titleStyle}>Khởi chiếu:</Text>
            <Text>{moment(data?.premiere).format('DD - MM - YYYY')}</Text>
            <Text style={styles.titleStyle}>Đánh giá: </Text>
            <Text>{data?.rating}/5</Text>
            <Text style={styles.titleStyle}>Thời lượng:</Text>
            <Text>{data?.duration} phút</Text>
            <Text style={styles.titleStyle}>Thể loại: </Text>
            <Text>
              {data?.filmCategories?.map((item: any) => item.category.name)}
            </Text>
            <Text style={styles.titleStyle}>Đạo diễn</Text>
            <Text>
              {data?.filmDirectors?.map((item: any) => item.director.fullName)}
            </Text>
          </View>
        </View>
        <Text style={styles.castTitle}>Diễn viên</Text>
        <FlatList
          horizontal
          data={data?.filmCasts}
          keyExtractor={(item: Object, index: number) => `${index}`}
          style={{ marginTop: 16 }}
          ListEmptyComponent={() => <Text>Không có dữ liệu</Text>}
          renderItem={({ item }) => (
            <View style={styles.castItem}>
              <Image
                source={{ uri: item?.cast?.avatar }}
                style={{ aspectRatio: 1, width: 75, borderRadius: 5 }}
              />
              <Text style={{ textAlign: 'center', marginTop: 8 }}>
                {item.cast.fullName}
              </Text>
            </View>
          )}
        />
        <Text style={{ marginTop: 16, fontWeight: 'bold', fontSize: 16 }}>
          Mô tả
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{ marginVertical: 16, fontWeight: 'bold', fontSize: 16 }}>
            Đánh giá
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TextInput
            style={styles.inputStyle}
            placeholder={'Nhập bình luận tại đây'}
            value={comment}
            onChangeText={onChange}
            multiline
          />
          <TouchableOpacity onPress={onSend} style={styles.sendButton}>
            <Text style={{ color: '#000' }}>Gửi</Text>
          </TouchableOpacity>
        </View>

        {reviews
          ?.map((item: any, index: number) => {
            return (
              <TouchableOpacity
                key={index.toString()}
                style={styles.commentItem}>
                <Image
                  source={{
                    uri:
                      item?.user?.avatar ||
                      'https://picsum.photos/200/300?random=1',
                  }}
                  style={styles.avatarStyle}
                />
                <View style={{ marginLeft: 16 }}>
                  <Text style={{ fontWeight: 'bold' }} numberOfLines={1}>
                    {item?.user?.displayName}
                  </Text>
                  <Text style={{ paddingRight: 60 }}>{item.content}</Text>
                </View>
              </TouchableOpacity>
            );
          })
          .reverse()}
      </ScrollView>
    </AppScreen>
  );
};

export default FilmDetailView;
