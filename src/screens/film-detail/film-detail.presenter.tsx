/**
 * @format
 */

import React, { useEffect, useState } from 'react';
import FilmDetailView from './film-detail.view';
import { Alert } from 'react-native';
import { API_URL } from '../../constants/constants';
import axios from 'axios';
import useAuth from '../../hooks/useAuth';
import AsyncStorage from '@react-native-community/async-storage';

const FilmDetailPresenter = ({ route }: any) => {
  const id: number = route.params.id;
  const { currentUser } = useAuth();

  const [comment, setComment] = useState<string>('');
  const [rating, setRating] = useState<number>(0);
  const [data, setData] = useState({});
  const [reviews, setReviews] = useState([]);

  const _getFilmDetail = async () => {
    try {
      const result = await axios.get(`${API_URL}/film/${id}/detail`);
      setData(result.data);
      setReviews(result.data.reviews);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  const _onSend = async () => {
    if (!comment.length) return;
    const payload = {
      content: comment,
    };

    try {
      const token = await AsyncStorage.getItem('token');
      if (!token) {
        Alert.alert('Lỗi', 'Chưa đăng nhập');
        return;
      }
      const headers = {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
      const profile = await axios.get(`${API_URL}/user/profile`, {
        headers,
      });
      const result: any = await axios.post(
        `${API_URL}/review/${profile.data.id}`,
        payload,
        {
          headers,
        },
      );
      setComment('');
      const newReview: any = [
        ...reviews,
        { id: result.data.id, content: result.data.content, user: currentUser },
      ];
      setReviews(newReview);
    } catch (e) {
      Alert.alert('Lỗi', e.message);
    }
  };

  useEffect(() => {
    _getFilmDetail();
  }, []);

  return (
    <FilmDetailView
      data={data}
      onSend={_onSend}
      onChange={setComment}
      comment={comment}
      reviews={reviews}
      rating={rating}
      onChangeRate={setRating}
    />
  );
};

export default FilmDetailPresenter;
