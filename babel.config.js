module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    [
      'module-resolver',
      {
        root: ['./'],
        extensions: ['.js', '.json', '.ts', '.tsx'],
        alias: {
          '@screens': './src/screens',
          '@router': './src/router',
          '@components': './src/components',
          '@assets': './assets',
          '@services': './src/services',
          '@common': './src/common',
          '@providers': './src/providers',
        },
      },
    ],
  ],
};
