module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
    curly: 'off',
  },
  settings: {
    'import/resolver': {
      'babel-module': {
        root: ['./'],
        extensions: ['.js', '.json'],
        alias: {
          '@assets': './assets',
          '@components': './src/components',
          '@screens': './src/screens',
          '@services': './src/services',
          '@providers': './src/providers',
          '@types': './src/types',
          '@hooks': './src/hooks',
        },
      },
    },
  },
};
